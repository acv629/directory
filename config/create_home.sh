#!/bin/bash

IFS=$'\n' users=($(cat users))

for user in "${users[@]}"; do
        runuser -l $user -c "mkdir /srv/home/$user"
done