# directory.proyecto-becarios-cert-2019.ml.

## Usuarios del servidor

Se definieron los siguientes usuarios con permisos de `sudo`, cada uno con una llave rsa diferente
con la cual conectarse a través de ssh

* alcast
* jonathandfmx
* dialid
* acv629
* valeriajz
* rodrigo

## Instalación del servidor.
---------------------------
### Instalación de OpenLDAP.

#### Preparación preeliminar.

Antes de instalar el OpenLDAP es necesario configurar correctamente el hostname del servidor.

El dominio general del proyecto de becarios es `proyecto-becarios-cert-2019.ml`. Por lo tanto se debe configurar el nombre del servidor bajo este dominio:

```
# hostnamectl set-hostname directory.proyecto-becarios-cert-2019.ml
```

También es necesario añadir una entrada al archivo `/etc/hosts` para que el servidor reconozca que las peticiones que lleguen a `directory.proyecto-becarios-cert-2019.ml` (en específico las propias) son para su propia IP `18.189.174.158`:

```
# nano /etc/hosts
```

Identificar la línea con IP `127.0.1.1` (la que NO tiene localhost) y cambiarla por:

```
127.0.1.1    directory.proyecto-becarios-cert-2019.ml    directory
```

Añadir la siguiente línea bajo la última entrada IPv4:

```
18.189.174.158    directory.proyecto-becarios-cert-2019.ml
```
### Instalación de LDAP.

Primero hay que instalar los siguientes paquetes:

  - **slapd**: Servidor de la implementación OpenLDAP. Incluye un daemon que escucha conexiones al sistema de directorios.
  - **ldap-utils**: Paquete que incluye comandos como *ldapsearch, ldapmodify, ldapadd*, etc, que permiten interactuar con el sistema de directorios y usar archivos ldif.
  - **ldapscripts**: Incluye scripts que permiten añadir y eliminar usuarios y grupos del sistema de directorios.

```
# apt -y install slapd ldap-utils ldapscripts
```

Inmediatamente al instalar los paquetes, se ejecutará un script de pre-instalación que pide la contraseña que usará el usuario administrador de LDAP. La contraseña a introducir aquí deberá ser segura, debido a que los servicios a instalar posteriormente serán accesibles desde la red pública hasta que se configure OpenVPN:

![alt text](img/1/image1.png "Configuración LDAP")

Debido a que estamos en **Debian 10 (Buster)**, la OpenLDAP genera la configuración de sistema de directorios automáticamente y crea el sistema bajo */etc/ldap/slapd.d*
Para verificar que todo va bien, revisamos el estado del servicio slapd, que debería estar activo:

```
# systemctl status slapd
```

También vamos a habilitar el servicio, para que inicie automáticamente al reiniciar el servidor:

```
# systemctl enable slapd
```

Revisamos que efectivamente existe el usuario admin:

```
# ldapsearch -W -D cn=admin,dc=proyecto-becarios-cert-2019,dc=ml -b dc=proyecto-becarios-cert-2019,dc=ml
```

Esto indica que se identifique con el DN `cn=admin`, `dc=proyecto-becarios-cert-2019`, `dc=ml` y que liste todas las entradas bajo en DN `dc=proyecto-becarios-cert-2019`, `dc=ml`. Esto debería listar dos entradas, la de la propia DN `dc=proyecto-becarios-cert-2019`, `dc=ml` y la DN del `admin`: `cn=admin`, `dc=proyecto-becarios-cert-2019`, `dc=ml`:

![alt text](img/1/image2.png "Creación de usuarios efectiva")

### Configuración de usuarios y grupos.

Por último, debido a que se usará este servidor LDAP para autenticar otros sistemas, es necesario agregar dos unidades organizacionales para que se guarden los usuarios y los grupos de unix respectivamente.

Para esto se crearán dos archivos *ldif*, los cuales se cargarán con `ldapadd`.
Crear el primer archivo en cualquier lugar:

```
# nano users.ldif
```

Dentro ponemos:

```
dn: ou=users,dc=proyecto-becarios-cert-2019,dc=ml
objectClass: organizationalUnit
ou: users
``` 

Este archivo creará el `OU` donde se guardarán los usuarios. Cargarlo con `ldapadd`:

```
# ldapadd -W -D cn=admin,dc=proyecto-becarios-cert-2019,dc=ml -f people.ldif
```

Siguiente creamos otro archivo en cualquier lugar:

```
# nano groups.ldif
```

Dentro ponemos:

```
dn: ou=groups,dc=proyecto-becarios-cert-2019,dc=ml
objectClass: organizationalUnit
ou: groups
``` 

Este archivo creará el `OU` donde se guardarán los grupos. Cargarlo con `ldapadd`:

```
# ldapadd -W -D cn=admin,dc=proyecto-becarios-cert-2019,dc=ml -f groups.ldif
```

Por último verificamos que la búsqueda con ldapsearch nos arroja las nuevas entradas:

```
# ldapsearch -W -D cn=admin,dc=proyecto-becarios-cert-2019,dc=ml -b dc=proyecto-becarios-cert-2019,dc=ml
```

![alt text](img/1/image3.png "Creación de grupos exitosa")

## Configuración de SSL
Se instala el certificado y la llave privada:

```
# install -D -o root -g ssl-cert -m 644 server.pem /etc/ssl/certs/server.pem
# install -D -o root -g ssl-cert -m 640 server.key /etc/ssl/private/server.key
```

Cambiamos los permisos del directorio ssl:

```
# chown root:ssl-cert /etc/ssl/private
chmod g+x /etc/ssl/private
```

Agregamos al usuario openldap al grupo ssl-cert para que pueda usar la llave privada:

```
# chmod -G ssl-cert -a openldap
```

Cambiamos en el archivo de configuración `/etc/default/slapd` donde va a escuchar el servicio de `slapd` para que escuche sin SSL solamente de manera local:

```
SLAPD_SERVICES="ldap://127.0.0.1:389/ ldaps://*:636/ ldapi:///"
```

Establecemos en `/etc/ldap/slapd.d/cn=config.ldif`, debajo de `olcToolThreads:`

```
olcTLSCertificateFile: /etc/ssl/certs/server.pem
olcTLSCertificateKeyFile: /etc/ssl/private/server.key
```

Rehacemos la base, y le ponemos los permisos adecuados

```
# slapindex -F /etc/ldap/slapd.d/
# chown -R openldap:openldap /var/lib/ldap/
```

Reiniciamos `slapd`

```
# systemctl restart slapd
```

--------------------------
##  Instalación de LDAP Account Manager.

Debido a que este servidor es un **Debian 10 (Buster)**, el paquete de instalación de `LAM` aún no se encuentra en los repositorios oficiales. Por esta razón se debe conseguir el **.deb** desde la página de `LAM`:

```
# wget http://prdownloads.sourceforge.net/lam/ldap-account-manager_6.8-1_all.deb
```

Una vez descargado el **.deb**, instalarlo:

```
# dpkg -i ldap-account-manager_6.8-1_all.deb
```

Esto intentará instalar `LAM`. Debido a las dependencias faltantes, la instalación quedará incompleta. Proseguir con el comando:

```
# apt --fix-broken install
```

El cual automáticamente listará e instalará las dependencias faltantes, además de acabar la instalación del **.deb** anterior. Al finalizar esto, se habrá instalado y configurado un servidor apache con `PHP 7.3` y `LAM`.

Verificamos que el servidor apache esté corriendo:

```
# systemctl status apache2
```

Habilitamos el servicio para que apache inicie al reiniciar el servidor:

```
# systemctl enable apache2
```

`LAM` se instala como configuración de `apache2`, no como sitio. El archivo de `LAM` es */etc/apache2/conf-available/ldap-account-manager.conf*. La raíz del sitio está en */usr/share/ldap-account-manager/*.

El sitio ya está habilitado, basta entrar desde el navegador a la *IP/lam*, i.e `http://18.189.174.158/lam`

![alt text](img/2/image2.png "Pagina del sitio LAM")

-----------------------------------------

## Instalación de Self Service Password.

Se instalará la versión actualmente estable de `SSP:1.3`.

Primero hay que obtener el **.deb**:

```
# wget http://ltb-project.org/archives/self-service-password_1.3-1_all.deb
```

Instalar normalmente:

```
# dpkg -i self-service-password_1.3-1_all.deb
```
Instalar el módulo de `PHP` *mbstring* necesario:

```
# apt -y install php7.3-mbstring
```

Reiniciar el `servidor apache` para cargar el módulo:

```
# systemctl reload apache2
```

El sitio se añadió al servidor en */etc/apache2/sites-available/self-service-password.conf*

----------------------------

## Configuración SSL para apache

Antes de proseguir la configuración de los servicios de LAM y SSP, hay que configurar SSL para el servidor.

Editar el archivo del sitio principal `/etc/apache2/sites-available/self-service-password.conf`

Envolver todo el contenido actual entre:

```
<IfModule mod_ssl.c>
...
</IfModule>
```

Cambiar `<VirtualHost *:80>` por `<VirtualHost *:443>`

Agregar dentro de este `VirtualHost`:

```
ServerName directory.proyecto-becarios-cert-2019.ml
SSLEngine on
SSLCertificateFile /etc/ssl/certs/server.pem
SSLCertificateKeyFile /etc/ssl/private/server.key
```

Agregamos un nuevo `VirtualHost` para redirigir todo el tráfico HTTP a HTTPS

```
<VirtualHost *:80>
        ServerName directory.proyecto-becarios-cert-2019.ml
        RewriteEngine On
        RewriteRule ^(.*)$ https://%{HTTP_HOST}$1
</VirtualHost>
```

Por último activamos el módulo SSL y reiniciamos apache:

```
a2enmod ssl
systemctl restart apache2
```

-------------------------------

## Configuración de SSP.

Habilitamos el sitio:

```
# a2ensite self-service-password.conf
```

Deshabilitamos el sitio default:

```
# a2dissite 000-default.conf
```

Cargamos los cambios de configuración en el servidor:

```
# systemctl reload apache2
```

Crear un archivo nuevo de configuración. No confundirse con el archivo con nombre parecido que ya está en la misma ruta:

```
# nano /usr/share/self-service-password/conf/config.inc.local.php
```
![alt text](img/3/image1.png "Creando archivo de configuración")

Asegurarse que el valor de la variable `$ldap_bindpw` sea el de la contraseña del admin de `LDAP`.

Entramos al servidor `http://18.189.174.158` y veremos:

![alt text](img/3/image2.png "Verificación de alta exitosa de servicio")

-----------------------------------------

## Seguridad en Apache

Lo primero que debemos es hacer es oculatar información de la página predeterminada, ya que es esta la que muestra los mensaj
es de error como muestra la siguiente imagen:

![alt text](img/ap/1.png "Mensaje no deseados")

Para eliminar estos mensajes que pueden representar una brecha de seguridad debemos ingresar y modificar el archivo 
```
# sudo nano /etc/apache2/conf-available/security.conf
```
Y cambiar los siguientes parametros:
```
ServerSignature Off
ServerTokens Prod
Header set X-Frame-Options: "sameorigin"
```
Reiniciamos el `servidor apache`
```
# service apache2 restart
```
Veridficar que en el archivo *apache2.conf* esten activados los archivos `.htaccess` del siguiente directorio así mismo como deshabilitar el acceso predeterminado.
```
<Directory /> 
    AllowOverride None 
    Require all denied
 </Directory>
 ```
-----------------------------------------

## Configurar LAM.

Una vez en el sitio, entrar al apartado *LAM configuration* en la esquina superior derecha. De aquí seleccionar *Edit server profiles*:

![alt text](img/2/image14.png "Sección de configuración")

Te pedirá una contraseña de `LAM`. Esta contraseña **NO** es la misma que la del **usuario admin** de nuestro `servidor LDAP`. La contraseña por defecto de `LAM` es ‘lam’, sin comillas.

![alt text](img/2/image6.png "Pagina de login")

Lo primero que debemos hacer una vez dentro es cambiar la contraseña del profile `lam`, que se encuentra al final de la página, y hacer click en *Save*:

![alt text](img/2/image3.png "Cambio de contraseña")

Esto nos redirigirá a la página de inicio.

Una vez más, entramos a la página de configuración del servidor, y esta vez cambiaremos las configuraciones del servidor en el apartado *Server settings*, cambiando el `DN` por el de nuestro `servidor LDAP`, y la dirección del servidor por nuestro *hostname*.

![alt text](img/2/image8.png "Actualización de configuración del servidor")

En la misma página bajamos al apartado *Security settings* y cambiamos el `DN` en *List of valid users* por el de nuestro usuario admin:

![alt text](img/2/image12.png "Cambiamos el administrador")

Después subimos al principio de la página y cambiamos de pestaña a *Account Types*. Aquí bajamos a las secciones *Users* y *Groups* y cambiamos los `DN` respectivos por los que creamos en nuestro `servidor LDAP`;**ou=People,dc=proyecto-becarios-cert-2019,dc=ml** y **ou=groups,dc=proyecto-becarios-cert-2019,dc=ml**

Notar el plural de **groups**:

![alt text](img/2/image9.png "Configuración de usuarios y grupos")

Por último, pasamos a la pestaña *Module settings* y en el apartado *Users Options* habilitamos *Set primary group as memberUid*: 

![alt text](img/2/image10.png "Configuración final")

Bajamos al final de la página y damos click en Save

Verificamos que en la página principal podemos entrar ahora con la contraseña de admin de LDAP y que podemos crear un grupo y un usuario (en ese orden). En el caso del usuario hay que entrar a todas las pestañas de la izquierda al menos una vez, y en la pestaña Shadow hay que habilitarlo con el botón. También hay que cambiar el RDN identifiercn por uid:
 
![alt text](img/2/image7.png )

![alt text](img/2/image11.png)

![alt text](img/2/image1.png)

![alt text](img/2/image4.png)

![alt text](img/2/image5.png)

Recuerden que todos los cambios requieren picarle al botón de **save** o no se guardarán los nuevos grupos y usuarios.

Podemos verificar en el `servidor LDAP` con `ldapsearch` que se creó el grupo y el usuario:

```
# ldapsearch -W -D cn=admin,dc=proyecto-becarios-cert-2019,dc=ml -b dc=proyecto-becarios-cert-2019,dc=ml
```

![alt text](img/2/image13.png "Verificación de usuarios y grupos agregados")

---------------------------------------
## Autenticar clientes.

El siguiente documento presenta los datos para configurar la autenticación de usuarios `UNIX` de un servidor **Debian 10 (Buster)** contra un `servidor LDAP`.

### Configurar PAM y NSS.
Para autenticar los usuarios se usará `libpam-ldapd` y `libnss-ldapd`. Esto permitirá al equipo consultar el `servidor LDAP` por los usuarios y grupos al momento de autenticarse.

Primero, configurar `debconf` para que la instalación de los paquetes nos muestre la mayor cantidad de opciones al configurarse:

```
# dpkg-reconfigure debconf
```

Aquí especificamos **Dialog** como interfaz de menú y ponemos la prioridad de las preguntas como **Low**.

Instalamos los paquetes necesarios:

```
# apt install libpam-ldapd
```

Nos preguntará la dirección del `servidor ldap`:

```
ldaps://directory.proyecto-becarios-cert-2019.ml:636
```

Nos preguntará el `DN` base del servidor:

```
dc=proyecto-becarios-cert-2019,dc=ml
```

En la autenticación de la `base LDAP` ponemos **None**.

En el uso de `StartTLS` ponemos **No**.

Seleccionamos con la tecla espacio los **servicios**:
  - Passwd
  - Group
  - Shadow
  - Aliases

Seleccionamos con la tecla de espacio los `perfiles PAM`:
  - UNIX authentication
  - LDAP authentication

Por último reiniciamos el servicio de `nscd`, usando específicamente el script:

```
# /etc/init.d/nscd restart
```
---------------------------------------

## Creación de Respaldos

### Slapcat

En este documento se presenta cómo crear respaldos de la configuración de un `servidor LDAP` cada **10 minutos**. 

El comando que nos muestra los parámetros con los que fue configurado un `servidor LDAP` es `slapcat`. Con la **opción** `-l` indicamos la ruta del archivo a donde queremos que sea enviada la salida del `slapcat`, en este caso el directorio del usuario respaldos. 

```
# slapcat -l /home/respaldos/config.ldif
```

### Crontab con slapcat

Para que los respaldos sean generados de manera continua es necesario realizar lo siguiente:

```
# sudo vi /etc/crontab
```

Agregar la siguientes líneas:

```
10  *    * * *   root    slapcat -l /home/respaldos/config.ldif
20  *    * * *   root    slapcat -l /home/respaldos/config.ldif
30  *    * * *   root    slapcat -l /home/respaldos/config.ldif
40  *    * * *   root    slapcat -l /home/respaldos/config.ldif
50  *    * * *   root    slapcat -l /home/respaldos/config.ldif
1   *    * * *   root    slapcat -l /home/respaldos/config.ldif
```

Con esto indicamos que el usuario `root` va a ejecutar el `slapcat` en el minuto **1**, **10**, **20**, **30**, **40** y **50** de cada hora, cualquier día del mes, cualquier mes y cualquier día de la semana. 

Otra agregando la siguiente línea en vez de las anteriores para indicar que ejecute el comando cada **10 minutos**: 

```
*/10  *    * * *   root    slapcat -l /home/respaldos/config.ldif
```

Por último para verificar que el servicio `cron` esté activo se ejecuta el comando:

```
#  sudo service --status-all
```

Si a la izquierda de `cron` aparece un signo **+** indica que el servicio está corriendo, si aparece **-** es necesario prender el servicio con el comando:

```
# sudo service cron start
```
## Configuración del firewall

### Configuración de iptables
Instalar `iptables-persistent`

Guardar en un archivo los siguientes comandos:

```
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -i lo -j ACCEPT

iptables -A INPUT -p tcp --dport 22 -j ACCEPT

iptables -A INPUT -s tonejito.cf -p tcp --match multiport --dports 443,80,636 -j ACCEPT
iptables -A INPUT -s becarios.tonejito.cf -p tcp --match multiport --dports 443,80,636 -j ACCEPT
iptables -A INPUT -s priv.becarios.tonejito.cf -p tcp --match multiport --dports 443,80,636 -j ACCEPT
iptables -A INPUT -s nagios.becarios.tonejito.cf -p tcp --match multiport --dports 443,80,636 -j ACCEPT
iptables -A INPUT -s aruba.tonejito.info -p tcp --match multiport --dports 443,80,636 -j ACCEPT
iptables -A INPUT -s vultr.tonejito.info -p tcp --match multiport --dports 443,80,636 -j ACCEPT
iptables -A INPUT -s ovh.tonejito.info -p tcp --match multiport --dports 443,80,636 -j ACCEPT
iptables -A INPUT -s h1p.tonejito.info -p tcp --match multiport --dports 443,80,636 -j ACCEPT
iptables -A INPUT -s tonejito.ovh -p tcp --match multiport --dports 443,80,636 -j ACCEPT
iptables -A INPUT -s tonejito.org -p tcp --match multiport --dports 443,80,636 -j ACCEPT
iptables -A INPUT -s tonejito.info -p tcp --match multiport --dports 443,80,636 -j ACCEPT
iptables -A INPUT -s 132.247.0.0/16 -p tcp --match multiport --dports 443,80,636 -j ACCEPT
iptables -A INPUT -s 132.248.0.0/16 -p tcp --match multiport --dports 443,80,636 -j ACCEPT


iptables -A INPUT -s 3.16.142.205 -p tcp --match multiport --dports 443,80,636 -j ACCEPT
iptables -A INPUT -s 18.189.227.64 -p tcp --match multiport --dports 443,80,636 -j ACCEPT
iptables -A INPUT -s 18.189.132.180 -p tcp --match multiport --dports 443,80,636 -j ACCEPT
iptables -A INPUT -s 13.59.79.168 -p tcp --match multiport --dports 443,80,636 -j ACCEPT
iptables -A INPUT -s 18.189.235.50 -p tcp --match multiport --dports 443,80,636 -j ACCEPT


iptables -A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT

iptables -A INPUT -p icmp --icmp-type echo-request -j ACCEPT


iptables -P INPUT DROP
```

Probar con `iptables-apply` que los cambios funcionen correctamente:

```
# iptables-apply -c ./archivo
```

Si todo sigue bien, guardar las reglas para que persistan cada reinicio

```
# iptables-save > /etc/iptables/rules.v4
```

### Configuración de fail2ban

Instalar `fail2ban`:
```
# apt install fail2ban
```

Habilitar el servicio:
```
# systemctl enable fail2ban
```

Editamos el archivo `/etc/fail2ban/jail.d/defaults-debian.conf` donde especificaremos que banee a aquellos que fallen autenticarse 4 veces, durante 1 hora.

```
[sshd]
enabled = true
bantime = 3600
maxentry = 4
```

Reiniciamos el servicio:
```
# systemctl restart fail2ban
```

## Creación de los usuarios
Se creará 1 usuario por cada becario en OpenLDAP:

### Montar home
Se montará el home que exportó el equipo `storage`:

```
# mount storage.proyecto-becarios-cert-2019.ml:/srv/home /srv/home
```

Agregamos a `/etc/fstab` la siguiente línea:
```
storage.proyecto-becarios-cert-2019.ml:/srv/home /srv/home nfs defaults 0 0
```

### Crear los usuarios

Usando LAM, se crearán los usuarios con un archivo de exel. Ver archivo adjunto en `config/`

### Creando los homes
Dada una archivo de los usuarios añadidos:
```
layala
rbaruch
alcastillo
arcastillo
oignacio
dmatuz
cnaranjo
fnovales
kportillo
aposadas
lresendiz
asaavedra
murbina
avalverde
jvilla
dxotla
lbarragan
lceron
jcolula
afernandez
ehernandez
mhernandez
mjimenez
jliberos
alopez
aluna
jmartinez
rmartinez
jperez
cramirez
rrodriguez
edhernandez
```

Se utilizó el siguiente script para crear los homes en el directorio montado:

```
#!/bin/bash

IFS=$'\n' users=($(cat users))

for user in "${users[@]}"; do
        runuser -l $user -c "mkdir /srv/home/$user"
done
```
----------------------------------

### Instalación de OpenVPN

Se instaló el paquete `openvpn`, y se subió el archivo de configuración `client.conf`
junto con las llaves y certificados necesarios a `/etc/openvpn/`. Por último
se inició y habilitó el servicio. La IP asignada (al momento de esta documentación)
fue la `10.0.8.18`

```
apt install openvpn
systemctl start openvpn@client
systemctl enable openvpn@client
```

```
client
dev tun
proto udp
remote-random
remote vpn.proyecto-becarios-cert-2019.ml 1194
remote 18.189.235.50 1194
resolv-retry infinite
nobind
persist-key
persist-tun
ca   /etc/openvpn/ca.crt
cert /etc/openvpn/client.crt
key  /etc/openvpn/client.key
cipher AES-256-CBC
comp-lzo
verb 3
ns-cert-type server
```
